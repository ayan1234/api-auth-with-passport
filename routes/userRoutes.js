const router = require('express').Router();
const passport = require('passport');
require('../auth/passport-initialize');

router.use(passport.authenticate('jwt', {session:false}));

router.get('/', (req, res, next) => {
    res.json({
        message: 'You made it',
        user: req.user,
        token: req.query.token
    })
})

module.exports = router;