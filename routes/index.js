const router = require('express').Router();
const userRoutes = require('./userRoutes');
const authRoutes = require('./authRoutes')
const winston = require('../config/winston-config');

router.use('/', authRoutes);
router.use('/user', userRoutes);

router.use((req, res, next) => {
    res.send('Page not found');
})

router.use((err, req, res, next) => {
    res.send('Something is broken');
});

module.exports = router;