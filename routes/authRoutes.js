const router = require('express').Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const winston = require('../config/winston-config');
require('../auth/passport-initialize');

router.post('/signup', passport.authenticate('signup', {session: false}), (req, res, next) => {
    res.json({
        'message': 'Sign up Successful',
        'user': req.user
    });
});

router.post('/login', (req, res, next) => {
    passport.authenticate('login', (err, user, info) => {
        try {
            if(err) {
                return next(err);
            }
            if(!user) {
                return res.json(info);
            }
            req.login(user, {session: false}, (err) => {
                if(err) {
                   return next(err);
                }
                let token = jwt.sign({user: user.email}, process.env.SECRET);
                return res.json({token});
            })
        } catch(err) {
            winston.error(err);
        }
    })(req, res, next);
})

module.exports = router;