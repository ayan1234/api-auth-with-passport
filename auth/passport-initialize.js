const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const JWTStrategy = require('passport-jwt').Strategy;
const  ExtractJwt = require('passport-jwt').ExtractJwt;
const jwt = require('jsonwebtoken');
const {createUser, findUser} = require('../model/user.da');
const winston = require('../config/winston-config');

passport.use('signup', new localStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, async (email, password, done) => {
        try{
            let user = await createUser(email, password);
            return done(null, user);
        } catch(err) {
            winston.error(err);
            return done(err);
        }
}));

passport.use('login', new localStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, async (email, password, done) => {
    try {
        let users = await findUser(email);
        if(!users.rowLength === 0) {
            return done(null, false, {message: 'Invalid email or password'});
        }
        let user = users.rows[0];
        if(user.password !== password) {
            return done(null, false, {message: 'Invalid email or password'});
        }
        return done(null, user);
    } catch(err) {
        winston.error(err);
        return done(err);
    }
}));

passport.use('jwt', new JWTStrategy({
    secretOrKey: process.env.SECRET,
    jwtFromRequest: ExtractJwt.fromUrlQueryParameter('token')
}, async (token, done) => {
        try {
            console.log(token);
            return done(null, token.user);
        } catch(err) {
            return done(err);
        }
}))