const winston = require('winston');
const path = require('path');

const option = {
    file: {
        level: 'info',
        filename: path.join(__dirname, 'logs/app.log'),
        handleExceptions: true,
        json: true,
        maxsize:  5242880,
        maxfiles: 5,
        colorsize: false
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorsize: true
    }
}

const logger = winston.createLogger({
    transports: [
        new winston.transports.File(option.file),
        new winston.transports.Console(option.console)
    ],
    exitOnError: false
});

module.exports = logger;