const express = require('express');
const app = express();
const http = require('http');
require('dotenv').config();
const routes = require('./routes');
const server = http.createServer(app);

app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.use('/', routes);

server.listen(process.env.PORT);

server.on('listening', () => {
    console.log('App is running on port 3000');
});

server.on('error', (err) => {
    console.log('Server throws error' + err);
})
