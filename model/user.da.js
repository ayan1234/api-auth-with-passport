const {CassandraClient} = require('./db');
const {Uuid} = require('cassandra-driver').types;
const winston = require('../config/winston-config');

const createUser = (email, password) => {
    let query = 'INSERT INTO user(id, email, password) VALUES (?, ?, ?)';
    let userId = Uuid.random();
    return CassandraClient.execute(query, [Uuid.random(), email, password])
            .then(() => {
                return {userId, email};
            })
            .catch(err => {
                winston.error(err);
            })
}

const findUser = (email) => {
    let query = 'SELECT email, password, id FROM user WHERE email = ?';
    return CassandraClient.execute(query, [email]);
}

module.exports = {
    createUser,
    findUser
}

